import logoSanbercode from './logo.png';
import './App.css';

function CheckBox(props) {
  return (
    <div className="checkBox-Container">
      <input type="checkbox" name="text" id="text" />
      <label for="text">Belajar {props.text}</label>
      <br />
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <div className="Card">
        <div className="logo">
          <img src={logoSanbercode} alt="Logo Sanbercode" />
        </div>

        <h1>THINGS TO DO</h1>
        <h3>During bootcamp in sanbercode</h3>
        <hr />
        <div className="check-Box">
          <CheckBox text="Git dan Cli" /><hr />
          <CheckBox text="HTML dan CSS" /><hr />
          <CheckBox text="Javascript" /><hr />
          <CheckBox text="React Js dasar" /><hr />
          <CheckBox text="React Js advance" /><hr />
        </div>
        <div className="centered-Button">
          <button>SEND</button>
        </div>
      </div>
    </div>
  );
}

export default App;